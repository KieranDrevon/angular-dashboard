import { Component } from '@angular/core';
import { ProductserviceService } from 'src/app/services/productservice.service';
import { Product } from 'src/app/models/product.model';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent {

  constructor(
    private productsService: ProductserviceService
  ) {}

  products: Product[] = []

    ngOnInit(): void {
      this.productsService.getallProducts().subscribe( products => {
        this.products = products
      });

    }

}
