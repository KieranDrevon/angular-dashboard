import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  Highcharts: typeof Highcharts = Highcharts; // required
  chartConstructor: string = 'chart'; // optional string, defaults to 'chart'
  chartOptions: Highcharts.Options = {
    series: [{
      data: [
        { name: 'Gi', y: this.getRandomNumber() },
        { name: 'Rashguard', y: this.getRandomNumber() },
        { name: 'Mouthguard', y: this.getRandomNumber() },
        { name: 'No-Gi Shorts', y: this.getRandomNumber() },
        { name: 'Grappling Dummy', y: this.getRandomNumber() }
      ],
      type: 'pie',
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b>: {point.y}'
      }
    }],
    title: { text: 'Top Products' }
  };
  updateFlag: boolean = false; // optional boolean
  oneToOneFlag: boolean = true; // optional boolean, defaults to false
  runOutsideAngular: boolean = false; // optional boolean, defaults to false

  getRandomNumber(): number {
    // get a random number between 1 and 10
    return Math.floor(Math.random() * 10) + 1;
  }
}


