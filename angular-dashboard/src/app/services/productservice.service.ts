import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../models/product.model'

@Injectable({
  providedIn: 'root'
})
export class ProductserviceService {

  private apiUrl: string = 'https://fakestoreapi.com/products';

  constructor(private http: HttpClient) { }

  getallProducts(): Observable<Product[]> {
    const products = this.http.get<Product[]>(this.apiUrl);
    return products;
  }
}
